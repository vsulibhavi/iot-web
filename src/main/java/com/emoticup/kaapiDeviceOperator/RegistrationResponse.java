package com.emoticup.kaapiDeviceOperator;

import javax.servlet.Registration;

public class RegistrationResponse {

    String userId;


    RegistrationResponse(String userId){

        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
