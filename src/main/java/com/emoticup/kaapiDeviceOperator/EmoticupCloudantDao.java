package com.emoticup.kaapiDeviceOperator;

import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmoticupCloudantDao implements EmoticupRepo {


    @Autowired
    CloudantClient cloudantClient;

    @Override
    public void registerUser(RegisterUser registerPojo) {


       System.out.println(cloudantClient.getAllDbs());
       Database emoticupDb =  cloudantClient.database("emoticup",false);
       if(!emoticupDb.contains(registerPojo.getEmailId()))
        emoticupDb.save(registerPojo);

    }


}
