package com.emoticup.kaapiDeviceOperator;


import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CloudantConfig {


    @Autowired
    CloudantClient cloudantClient;
    @Bean
    public CloudantClient getCloudantClient(){

     return    ClientBuilder.account("15b663ae-cb29-4058-b4f6-da405ada9618-bluemix")
                .username("15b663ae-cb29-4058-b4f6-da405ada9618-bluemix")
                .password("243ae00851c43122fd762376e03b6758b9c1830172e14da15f1213d9a3af03b0")
                .build();

    }

    @Bean
    public Database getEmoticupDataBase(){

        Database emoticupDb = cloudantClient.database("emoticup",false);
        return emoticupDb;

    }


}
