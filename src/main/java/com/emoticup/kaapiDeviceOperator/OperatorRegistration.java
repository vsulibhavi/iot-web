package com.emoticup.kaapiDeviceOperator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OperatorRegistration {

    @Autowired
    EmoticupRepo emoticupRepo;

    @RequestMapping(value = "/registerUser",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public ResponseEntity search(@RequestBody RegisterUser registerUser){

        emoticupRepo.registerUser(registerUser);
       return ResponseEntity.ok(new RegistrationResponse(registerUser.emailId));

    }


}
