package com.emoticup.iot;

import com.vaadin.annotations.Theme;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.emoticup.*")
public class IotApplication{

	VerticalLayout verticalLayout = new VerticalLayout();

	public static void main(String[] args) {


		SpringApplication.run(IotApplication.class, args);
	}
}
