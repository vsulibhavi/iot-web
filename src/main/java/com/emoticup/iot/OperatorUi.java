package com.emoticup.iot;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;

@SpringUI
@Theme("valo")
public class OperatorUi extends UI{

    private VerticalLayout layout;

    @Override
    protected void init(VaadinRequest vaadinRequest) {

            setupLayout();
            addHeader();
            addForm();
            addTodoList();
            addActionButton();


    }

    private void addActionButton() {
    }

    private void addHeader() {

        Label header = new Label("Operator");
        layout.addComponent(header);
    }

    private void setupLayout() {

        layout = new VerticalLayout();
        setContent(layout);

    }
    class TextProperty{

        String textProperty;

        public String getTextProperty() {
            return textProperty;
        }


        public void setTextProperty(String textProperty) {
            this.textProperty = textProperty;
        }
    }

    private void addForm(){

        HorizontalLayout formLayout  = new HorizontalLayout();
        formLayout.setSpacing(true);
        formLayout.setWidth("80%");
        TextField textField = new TextField("write anything");
         Binder.Binding binder = new Binder().forField(textField).bind(new ValueProvider<TextProperty,String>() {
            @Override
            public String apply(TextProperty o) {
                return o.getTextProperty();
            }
        }, new Setter<TextProperty,String>() {
            @Override
            public void accept(TextProperty o, String o2) {
                    o.setTextProperty(o2);
            }
        });
        formLayout.addComponent(textField);
        layout.addComponent(formLayout);

    }
    private void addTodoList(){



    }



}
